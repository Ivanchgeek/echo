package gametransfer

//Transfer - streams data between two clients
type Transfer struct {
	connections map[*Client]bool
	attach      chan *Client
	deattach    chan *Client
}

//NewTransfer - create new Transfer
func NewTransfer() *Transfer {
	return &Transfer{
		connections: make(map[*Client]bool),
		attach:      make(chan *Client),
		deattach:    make(chan *Client),
	}
}

//Run Transfer
func (t *Transfer) Run() {
	for {
		select {
		case client := <-t.attach:
			t.connections[client] = true
		case client := <-t.deattach:
			if _, ok := t.connections[client]; ok {
				delete(t.connections, client)
				close(client.send)
			}
		}
	}
}
