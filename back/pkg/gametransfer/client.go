package gametransfer

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/google/uuid"
	"github.com/gorilla/websocket"
)

//Message - message to peer
type Message struct {
	Payload     map[string]string
	MessageType string
}

const (
	writeWait          = 10 * time.Second
	pongWait           = 60 * time.Second
	pingPeriod         = (pongWait * 9) / 10
	maxMessageSize     = 512
	typeServiceMessage = "service"
	typeCommandMessage = "cmd"
)

var (
	lobbies = make(map[string]*Client)
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  4096,
	WriteBufferSize: 4096,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

//Client - web log view client connection
type Client struct {
	transfer         *Transfer
	conn             *websocket.Conn
	send             chan []byte
	connectTimeStamp time.Time
	target           *Client
}

func (c *Client) readPump() {
	defer func() {
		c.transfer.deattach <- c
		c.conn.Close()
	}()
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error { c.conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	for {
		msgType, data, err := c.conn.ReadMessage()
		if err != nil {
			break
		}
		if msgType != websocket.PingMessage && msgType != websocket.PongMessage && c.target != nil {
			message := Message{
				MessageType: typeCommandMessage,
				Payload:     map[string]string{"data": string(data)},
			}
			messageJS, _ := json.Marshal(message)
			if c.transfer.connections[c.target] {
				c.target.send <- messageJS
			} else {
				break
			}
		}
	}
}

func (c *Client) writePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.conn.Close()
	}()
	for {
		select {
		case message, ok := <-c.send:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}
			w, err := c.conn.NextWriter(websocket.TextMessage)
			if err != nil {
				return
			}
			w.Write(message)
			n := len(c.send)
			for i := 0; i < n; i++ {
				w.Write(<-c.send)
			}
			if err := w.Close(); err != nil {
				return
			}
		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

//Attach - attach client to transfer, returns lobby id
func Attach(transfer *Transfer, w http.ResponseWriter, r *http.Request, lobby string) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
	client := &Client{
		transfer:         transfer,
		conn:             conn,
		send:             make(chan []byte, 256),
		connectTimeStamp: time.Now(),
		target:           nil}
	if lobby == "init" {
		lobbyID := uuid.New().String()
		lobbies[lobbyID] = client
		tokenMessage := Message{
			MessageType: typeServiceMessage,
			Payload:     map[string]string{"type": "token", "token": lobbyID},
		}
		token, _ := json.Marshal(tokenMessage)
		client.send <- token
	} else {
		if lobbies[lobby] != nil {
			client.target = lobbies[lobby]
			lobbies[lobby].target = client
			connectMessage := Message{
				MessageType: typeServiceMessage,
				Payload:     map[string]string{"type": "connect"},
			}
			connectJS, _ := json.Marshal(connectMessage)
			lobbies[lobby].send <- connectJS
			delete(lobbies, lobby)
		} else {
			errorMessage := Message{
				MessageType: typeServiceMessage,
				Payload:     map[string]string{"type": "error", "error": "no such lobby"},
			}
			errorJS, _ := json.Marshal(errorMessage)
			client.send <- errorJS
		}
	}
	client.transfer.attach <- client
	go client.writePump()
	go client.readPump()
}
