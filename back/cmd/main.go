package main

import (
	"flag"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"gitlab.com/Ivanchgeek/echo/pkg/gametransfer"
)

var (
	transfer *gametransfer.Transfer
)

func init() {
	transfer = gametransfer.NewTransfer()
	go transfer.Run()
}

func main() {
	port := flag.String("port", "8080", "server port")
	flag.Parse()

	r := mux.NewRouter()
	r.HandleFunc(
		"/lobby/{token}",
		func(w http.ResponseWriter, r *http.Request) {
			token := mux.Vars(r)["token"]
			gametransfer.Attach(transfer, w, r, token)
		},
	)
	r.PathPrefix("/faq").Handler(http.StripPrefix("/faq", http.FileServer(http.Dir("./front/"))))
	r.PathPrefix("/game").Handler(http.StripPrefix("/game", http.FileServer(http.Dir("./front/"))))
	r.PathPrefix("/result").Handler(http.StripPrefix("/result", http.FileServer(http.Dir("./front/"))))
	r.PathPrefix("/").Handler(http.FileServer(http.Dir("./front/")))

	if err := http.ListenAndServe(":"+*port, cors.Default().Handler(r)); err != nil {
		panic(err)
	}
}
