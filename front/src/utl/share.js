import React from "react"
import {store} from "../store/configureStore"

class ShareDom extends React.Component {
    render() {
        return (
            // <input type="text" style={{width: "1px", height: "1px", position: "absolute", top: "0", left: "-20px"}} id="input-clipboard"/>
            <div style={{width: "100vw", height: "100px", position: "absolute", top: "0", left: "0px", userSelect: "all"}} id="clipboard"></div>
        )
    }
}

export const share = content => {
    if ("share" in navigator) {
        try {
            navigator.share(content)
        } catch (e) {
            shareClipboard(content.url)
        } 
    } else {
        shareClipboard(content.url)
    }
}

function shareClipboard(content) {
    var clipboard = document.getElementById("clipboard")
    clipboard.innerText = content
    var range = document.createRange()
    range.selectNode(clipboard)
    window.getSelection().addRange(range)
    document.execCommand("copy")
    window.getSelection().removeRange(range)
    clipboard.innerText = ""
    store.dispatch({type: "Notify", value: "Copied to clipboard."})
}

export default ShareDom