import {store} from "../store/configureStore"
import {hash} from "hash-it"

var apiURL = `${window.location.protocol.replace("http", "ws")}//${window.location.hostname}${(!process.env.NODE_ENV || process.env.NODE_ENV === 'development') ? ":8080" : ""}`
var signaling

export const InitLobby = () => {
    return dispatch => {
        dispatch({type: "StartLoading"})
        dispatch({type: "SetIAmLobby"})
        dispatch({type: "SetGameImpulsing", value: true})
        signaling = new WebSocket(`${apiURL}/lobby/init`)
        signaling.onerror = e => {
            console.error(`signaling: ${e}`)
            dispatch({type: "Notify", value: "network error"})
        }
        signaling.onmessage = message => {
            var data
            try {
                data = JSON.parse(message.data)
            } catch {
                data = {
                    MessageType: "ignore"
                }
            }
            switch (data.MessageType) {
                case "service": 
                    switch(data.Payload["type"]) {
                        case "token":
                            console.log("lobby token: ", data.Payload["token"])
                            dispatch({type: "SetLobbyToken", value: data.Payload["token"]})
                            dispatch({type: "StopLoading"})
                            dispatch({type: "SetGameImpulsing", value: false})
                            break
                        case "connect":
                            console.log("connection to lobby detected")
                            dispatch({type: "StartLoading"})
                            dispatch({type: "SetGamePrepairing", value: true})
                            signaling.send(JSON.stringify({type: "connected"}))
                            signaling.send(JSON.stringify({
                                type: "BlockerTimeoutSettingStepOne",
                                timestamp: new Date()
                            }))
                            break;
                        case "error":
                            console.error(data.Payload["error"])
                            break
                        default:
                            console.error(`wrong service message type "${data.Payload["type"]}"`)
                    }
                    break;
                case "cmd":
                    var command 
                    try {
                        command = JSON.parse(data.Payload["data"])
                    } catch(e) {
                        console.error("bad cmd format (" + e + ")")
                        break
                    }
                    CMDMessageHandle(command)
                    break;
                case "ignore":
                    break
                default:
                    console.error(`wrong service message type "${data.MessageType}"`)
            }
        }
    }
}

export const JoinLobby = lobby => {
    return dispatch => {
        dispatch({type: "StartLoading"})
        dispatch({type: "SetGameImpulsing", value: true})
        signaling = new WebSocket(`${apiURL}/lobby/${lobby}`)
        signaling.onopen = () => {
            signaling.send(JSON.stringify({type: "connected"}))
        }
        signaling.onerror = e => {
            console.error(`signaling: ${e}`)
            dispatch({type: "Notify", value: "network error"})
        }
        signaling.onmessage = message => {
            var data
            try {
                data = JSON.parse(message.data)
            } catch {
                data = {
                    MessageType: "ignore"
                }
            }
            switch (data.MessageType) {
                case "service": 
                    switch(data.Payload["type"]) {
                        case "error": {
                            switch(data.Payload["error"]) {
                                case "no such lobby":
                                    dispatch({type: "Notify", value: "There is no such lobby."})
                                    signaling.close()
                                    break;
                                default:
                                    console.error(data.Payload["error"])
                                    dispatch({type: "Notify", value: "Unexpected error occured."})
                            }
                            break;
                        }
                        default:
                            console.error(`wrong service message type "${data.Payload["type"]}"`)
                    }
                    break;
                case "cmd":
                    var command 
                    try {
                        command = JSON.parse(data.Payload["data"])
                    } catch(e) {
                        console.error("bad cmd format")
                        break
                    }
                    if (command.type === "connected") {
                        signaling.send(JSON.stringify({type: "connected"}))
                    }
                    CMDMessageHandle(command)
                    break;
                case "ignore":
                    break
                default:
                    console.error(`wrong message type "${data.MessageType}"`)
            }
        }
    }
}

function CMDMessageHandle(cmd) {
    switch (cmd.type) {
        case "connected":
            console.log("starting game")
            store.dispatch({type: "HardStopLoading"})
            store.dispatch({type: "SetGamePrepairing", value: false})
            store.dispatch({type: "SetGameReady", value: true})
            break
        case "BlockerTimeoutSettingStepOne":
            signaling.send(JSON.stringify({
                type: "BlockerTimeoutSettingStepTwo",
                timestamp: cmd.timestamp
            }))
            break
        case "BlockerTimeoutSettingStepTwo":
            var timeout = (new Date() - new Date(cmd.timestamp)) / 2
            store.dispatch({type: "SetBlockerTimeout", value: timeout})
            signaling.send(JSON.stringify({
                type: "BlockerTimeoutSettingStepThree",
                timeout: timeout
            }))
            break
        case "BlockerTimeoutSettingStepThree":
            store.dispatch({type: "SetBlockerTimeout", value: cmd.timeout})
            break
        case "rotation":
            store.dispatch({type: "CallUpdate", updatePack: {
                ...cmd,
                target: "remote"
            }})
            break
        case "shoot":
            store.dispatch({type: "CallUpdate", updatePack: {
                ...cmd,
                target: "remote"
            }})
            break
        case "sync": 
            store.dispatch({type: "CallUpdate", updatePack: {
                ...cmd
            }})
            break
        case "imitation": 
            store.dispatch({type: "CallUpdate", updatePack: {
                ...cmd
            }})
            break
        case "pushHash":
            store.dispatch({type: "CallSyncUpdate", syncPack: {
                ...cmd,
                blocked: store.getState().game.blockersCount > 0
            }})
            break
        case "hashRequest":
            store.dispatch({type: "CallSyncUpdate", syncPack: {
                ...cmd
            }})
            break
        default:
            console.error("unexpected peer cmd type")
    }
}

export const SetRotation = angle => {
    BlockerLyfecycle()
    SetRotationRemote(angle)
    var cmd = {
        type: "rotation",
        angle: angle,
        target: "self"
    }
    store.dispatch({type: "CallUpdate", updatePack: cmd})
}

const SetRotationRemote = angle => {
    if (signaling !== undefined) {
        signaling.send(JSON.stringify({
            type: "rotation",
            angle: angle
        }))
    }
}

export const Shoot = () => {
    BlockerLyfecycle()
    ShootRemote()
    var cmd = {
        type: "shoot", 
        target: "self"
    }
    store.dispatch({type: "CallUpdate", updatePack: cmd})
}

const ShootRemote = () => {
    if (signaling !== undefined) {
        signaling.send(JSON.stringify({
            type: "shoot"
        }))
    }
}

export const CallRemoteImitation = (width, height) => {
    if (signaling !== undefined) {
        signaling.send(JSON.stringify({
            type: "imitation",
            width: width,
            height: height
        }))
    }
}

export const SyncToRemote = state => {
    if (signaling !== undefined) {
        signaling.send(JSON.stringify({
            type: "sync",
            state: state
        }))
    }
}

export const RequetStateHash = () => {
    if (signaling !== undefined) {
        signaling.send(JSON.stringify({
            type: "hashRequest"
        }))
    }
}

export const PushStateHash = state => {
    if (signaling !== undefined) {
        signaling.send(JSON.stringify({
            type: "pushHash",
            hash: hash(state),
            timestamp: state.timer
        }))
    }
}

const BlockerLyfecycle = () => {
    store.dispatch({type: "AddBlocker"})
    setTimeout(() => {
        store.dispatch({type: "RemoveBlocker"})
    }, store.getState().networking.BlockerTimeout);
}