import React from "react"
import "../../styles/Notifications.sass"
import {connect} from "react-redux"

class Notifications extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            active: false
        }
        this.subActive = false
    }
    
    componentDidUpdate = () => {
        if (!this.subActive && this.props.content !== "") {
            this.subActive = true
            this.setState({active: true})
            setTimeout(() => {
                this.setState({active: false})
                this.props.dispatch({type: "Notify", value: ""})
                setTimeout(() => {
                    this.subActive = false
                }, 200)
            }, 2500)
        }
    }

    render() {
        return(
            <div className={`notifications ${this.state.active ? "active" : ""}`}>
                <span>{this.props.content}</span>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    content: state.notifications.content
});
export default connect(mapStateToProps)(Notifications)