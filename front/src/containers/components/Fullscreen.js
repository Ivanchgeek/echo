import React from "react"
import "../../styles/Fullscreen.sass"
import {connect} from "react-redux"

const fsflag = !document.cookie.includes("echofs=no")

class Fullscreen extends React.Component {

    constructor(props) {
        super(props);
        console.log("fullscreen manager active state: ", fsflag)
        if (fsflag) {
            this.props.dispatch({type: "SetFullscreenToggled", value: document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen})
            document.documentElement.onfullscreenchange = this.watchFullScreen
            document.onfullscreenchange = this.watchFullScreen
            document.addEventListener('fullscreenchange', this.watchFullScreen, false);
            document.addEventListener('mozfullscreenchange', this.watchFullScreen, false);
            document.addEventListener('MSFullscreenChange', this.watchFullScreen, false);
            document.addEventListener('webkitfullscreenchange', this.watchFullScreen, false);
        } else {
            this.props.dispatch({type: "SetFullscreenToggled", value: true})
        }
    }

    watchFullScreen = event => {
        this.props.dispatch({type: "SetFullscreenToggled", value: document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen})
    }

    render() {
        if (!fsflag) return(<></>)
        if (!this.props.fullscreen && !(window.location.pathname === "/" || window.location.pathname === "/faq" || window.location.pathname === "/result")) {
            return (
                <div className="fullscreen" onClick={toggleFullScreen}>
                    <h1>Fullscreen mode is required. Tap to toggle it.</h1>
                </div>
            )
        }
        return (<></>)
    }
}

export const toggleFullScreen = () => {
    if (fsflag) {
        var docElm = document.documentElement
        if (docElm.requestFullscreen) 
            docElm.requestFullscreen()
        else{
            if (docElm.mozRequestFullScreen) 
                docElm.mozRequestFullScreen()
            else{
                if (docElm.webkitRequestFullScreen)
                    docElm.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT)
            }
        }
    }
}

const mapStateToProps = (state) => ({
    fullscreen: state.fullscreen.toggled
});
export default connect(mapStateToProps)(Fullscreen)