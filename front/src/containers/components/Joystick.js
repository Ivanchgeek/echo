import React from "react" 
import "../../styles/Joystick.sass"
import {connect} from "react-redux"
import {SetRotation, Shoot} from "../../actions/networking"

class Joystick extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            active: false,
            center: {
                x: 0,
                y: 0
            }
        }
        this.mounted = false
        window.addEventListener("touchstart", this.touchManager)
        window.addEventListener("touchmove", this.touchManager)
        window.addEventListener("touchend", this.touchManager)
    }

    componentDidMount = () => {
        this.mounted = true
    }

    touchManager = event => {
        if (this.mounted && this.props.active) {
            switch(event.type) {
                case "touchstart":
                    if (this.props.ready) {
                        this.setState({
                            active: true,
                            center: {
                                x: event.touches[0].pageX,
                                y: event.touches[0].pageY
                            }
                        })
                    }
                    break
                case "touchmove":
                    let x = event.touches[0].pageX - this.state.center.x
                    let y = this.state.center.y - event.touches[0].pageY
                    let angle = Math.atan(x / y) * 180 / Math.PI
                    if ((x > 0 && y < 0) || (x <= 0 && y < 0)) {
                        angle += 180
                    }
                    if (x < 0 && y >= 0) {
                        angle += 360
                    }
                    if (!this.props.lobby) {
                        angle += 180
                    }
                    SetRotation(angle)
                    break
                case "touchend":
                    Shoot()
                    this.setState({active: false})
                    break
                default:
                    break
            }
        }
    }

    componentWillUnmount = () => {
        this.mounted = false
    }

    render() {
        return (
            <div className="joystick" style={{
                top: this.state.center.y + "px", 
                left: this.state.center.x + "px",
                display: this.state.active ? "block" : "none"
            }}/>
        )
    }
}

const mapStateToProps = (state) => ({
    lobby: state.game.lobby
});
export default connect(mapStateToProps)(Joystick)