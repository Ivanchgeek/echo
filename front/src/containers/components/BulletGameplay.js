import React from "react"

class BulletGameplay extends React.Component {

    componentDidMount = () => {
        this.interval = setInterval(() => {
            if (this.refs.svg !== undefined) this.refs.svg.style.opacity = 0
            setTimeout(() => {
                if (this.refs.svg !== undefined) this.refs.svg.setCurrentTime(0)
                setTimeout(() => {
                    if (this.refs.svg !== undefined) this.refs.svg.style.opacity = 1
                }, 200);
            }, 200);
        }, 3500);
    }

    componentWillUnmount = () => {
        clearInterval(this.interval)
    }

    render() {
        return (
            <svg width="100vw" height="96" viewBox="0 0 409 96" fill="none" xmlns="http://www.w3.org/2000/svg" ref="svg" style={{transition: ".2s"}}>
                <rect x="270" y="41" width="13.889" height="13.889" fill="white">
                    <animate attributeName="x" from="270" to="155" dur="2s" begin="0s" repeatCount="1" id="shoot"/>
                </rect>
                <rect x="105" y="23" width="50" height="50" fill="white">
                    <animate attributeName="opacity" from="1" to="0" dur=".2s" begin="shoot.end" repeatCount="1" fill="freeze"/>
                </rect>
                <rect x="124.681" y="42.6816" width="11" height="11" stroke="#4431B8" strokeWidth="2"/>
                <rect x="121.5" y="23" width="17" height="5.5" fill="#4431B8"/>
                <rect x="105" y="23" width="50" height="50" stroke="white" strokeDasharray="0, 15">
                    <animate attributeName="stroke-dasharray" from="0 15" to="10 15" dur=".5s" begin="shoot.end" repeatCount="1" fill="freeze"/>
                </rect>
                <rect x="253" y="23" width="50" height="50" fill="white"/>
                <rect x="272.681" y="42.6816" width="11" height="11" stroke="#CF3131" strokeWidth="2"/>
                <rect x="253" y="39.5" width="5.5" height="17" fill="#CF3131"/>
            </svg>
        )
    }
}

export default BulletGameplay