import React from "react"

class RotateGameplay extends React.Component {
    render() {
        return (
            <svg width="100vw" height="96" viewBox="0 0 409 96" fill="none" xmlns="http://www.w3.org/2000/svg" style={{overflow: "visible"}}>
                <circle cx="277.5" cy="48.5" r="4.5" fill="white" fillOpacity="0.51"/>
                <rect x="103" y="23" width="50" height="50" fill="white">
                    <animateTransform attributeName="transform" type="rotate" from="360 128 48" to="0 128 48" dur="4s" repeatDur="indefinite"/>
                </rect>
                <rect x="119.5" y="23" width="17" height="5.5" fill="#4431B8">
                    <animateTransform attributeName="transform" type="rotate" from="360 128 48" to="0 128 48" dur="4s" repeatDur="indefinite"/>
                </rect>
                <rect x="122" y="43" width="11" height="11" stroke="#4431B8" strokeWidth="2">
                    <animateTransform attributeName="transform" type="rotate" from="360 127.5 48.5" to="0 127.5 48.5" dur="4s" repeatDur="indefinite"/>
                </rect>
                <circle cx="278" cy="48" r="30.5" stroke="white" strokeOpacity="0.51" strokeDasharray="10 5"/>
                <path transform="translate(-30.5, 0)" d="M280.562 26.5687V21.7494M280.562 21.7494V18.1408C280.544 17.2282 279.513 17 279 17C277.843 17.0373 277.584 17.7761 277.599 18.1408V29.5022L275.624 27.4302C274.429 26.2381 273.641 26.592 273.396 26.918C272.679 27.3091 273.097 28.4157 273.396 28.9202L278.61 38H286.741C288.707 38 289.061 35.3304 288.992 33.9956V26.5687V25.3581C288.992 24.7435 287.966 24.5588 287.453 24.5432C286.406 24.4874 286.19 24.8381 286.213 25.0205M280.562 21.7494C280.562 21.4235 280.82 20.7809 281.849 20.8182C282.331 20.8027 283.3 20.9672 283.319 21.7494V23.4723M283.319 26.5687V23.4723M283.319 23.4723C283.319 23.2162 283.585 22.7133 284.651 22.7506C285.156 22.7118 286.176 22.8018 286.213 23.4723V25.0205M286.213 26.5687V25.0205" stroke="white" strokeWidth="2">
                    <animateMotion 
                        dur="4s"
                        begin="0s"
                        repeatCount="indefinite"
                        path="M62 29.0335C59.2 5.4335 39.8333 0.533504 30.5 1.0335C7.7 2.6335 1.33333 20.3668 1 29.0335C1.4 54.2335 20.8333 61.5335 30.5 62.0335C58.1 61.6335 63 39.8668 62 29.0335Z"/>
                </path>
            </svg>
        )
    }
}

export default RotateGameplay