import React from "react"

class RecoilGameplay extends React.Component {

    componentDidMount = () => {
        this.interval = setInterval(() => {
            if (this.refs.svg !== undefined) this.refs.svg.style.opacity = 0
            setTimeout(() => {
                if (this.refs.svg !== undefined) this.refs.svg.setCurrentTime(0)
                setTimeout(() => {
                    if (this.refs.svg !== undefined) this.refs.svg.style.opacity = 1
                }, 200);
            }, 200);
        }, 1800);
    }

    componentWillUnmount = () => {
        clearInterval(this.interval)
    }

    render() {
        return (
            <svg width="100vw" height="96" viewBox="0 0 409 96" fill="none" xmlns="http://www.w3.org/2000/svg" ref="svg" style={{transition: ".2s"}}>
                <rect x="125" y="55" width="13.889" height="13.889" transform="rotate(180 165.889 55)" fill="white">
                    <animate attributeName="x" from="125" to="420" dur="1s" fill="freeze" begin=".3s"/>
                </rect>
                <rect x="229" y="73" width="50" height="50" transform="rotate(180 229 73)" fill="white">
                    <animate attributeName="x" from="229" to="150" dur="1.8s" fill="freeze" begin=".3s"/>
                </rect>
                <rect x="274" y="89.5" width="5.5" height="17" transform="rotate(180 229 73)" fill="#4431B8">
                    <animate attributeName="x" from="274" to="195" dur="1.8s" fill="freeze" begin=".3s"/>
                </rect>
                <rect x="209.319" y="53.3184" width="11" height="11" transform="rotate(180 209.319 53.3184)" stroke="#4431B8" strokeWidth="2">
                    <animate attributeName="x" from="209.319" to="130.319" dur="1.8s" fill="freeze" begin=".3s"/>
                </rect>
            </svg>
        )
    }
}

export default RecoilGameplay