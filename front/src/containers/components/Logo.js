import React from "react"

class Logo extends React.Component {

    animate = () => {
        this.refs.trigger.beginElement()
    }

    render() {
        return (
            <svg width={this.props.width} height={this.props.height} viewBox="0 0 156 156" fill="none" xmlns="http://www.w3.org/2000/svg" className="logo">
                <rect x="33" y="96" width="25" height="25" fill="white" opacity="0">
                    <animate attributeName="opacity" from="0" to="1" dur="0.3s" begin="indefinite" id="first" ref="trigger" fill="freeze" />
                </rect>
                <rect x="100" y="96" width="25" height="25" fill="white" opacity="0">
                    <animate attributeName="opacity" from="0" to="1" dur="0.3s" begin="second.end" id = "third" fill="freeze" />
                </rect>
                <rect x="100" y="36" width="25" height="25" fill="white" opacity="0">
                    <animate attributeName="opacity" from="0" to="1" dur="0.3s" begin="second.end" fill="freeze" />
                </rect>
                <rect x="67" y="36" width="25" height="25" fill="white" opacity="0" id="snake-swanpoint">
                    <animate attributeName="opacity" from="0" to="1" dur="0.3s" begin="first.end" id="second" fill="freeze" />
                </rect>
                <rect x="33" y="36" width="25" height="25" fill="white" opacity="0">
                    <animate attributeName="opacity" from="0" to="1" dur="0.3s" begin="first.begin" fill="freeze" />
                </rect>
                <rect x="50" y="105" width="7" height="7" fill="white" opacity="0">
                    <animate attributeName="opacity" from="0" to="1" dur=".001s" begin="third.end" fill="freeze" />
                    <animate attributeName="x" from="50" to="85" dur=".4s" begin="third.end" fill="freeze" />
                </rect>
                <g clipPath="url(#clip0)">
                    <path d="M35.432 67.9643V76.9643H51.4966V78.9286H35.432V89.0357H51.4966V91H33.4858V66H51.4966V67.9643H35.432ZM58.2413 91V66H74.4828V67.9643H60.1874V89.0357H74.4828V91H58.2413ZM99.785 66V91H97.8389V78.9286H82.8004V91H80.8543V66H82.8004V76.9643H97.8389V66H99.785ZM124.527 91H106.163V66H124.527V91ZM108.109 67.9643V89.0357H122.581V67.9643H108.109Z" fill="white" />
                </g>
                <defs>
                    <clipPath id="clip0">
                        <rect x="33" y="66" width="92" height="25" fill="white" opacity="0" />
                    </clipPath>
                </defs>
            </svg>
        )
    }
}

export default Logo