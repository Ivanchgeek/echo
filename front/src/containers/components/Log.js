import React from "react"
import {connect} from "react-redux"
import {store} from "../../store/configureStore"

class Log extends React.Component {
    render() {
        if (process.env.NODE_ENV === 'development') {
            return (
                <div ref="log" style={{
                    position: "fixed",
                    width: "50vw",
                    height: "max-content",
                    top: "5px",
                    right: "0",
                    color: "red",
                    zIndex: "100",
                    fontSize: "18px",
                    textAlign: "right",
                    paddingRight: "10px",
                    pointerEvents: "none",
                }}>
                    {this.props.lines.map((line, id) => {
                        return (
                            <p key={id} style={{color: "red"}}>{line}</p>
                        )
                    })}
                </div>
            )
        } 
        return (<></>)
    }
}

export const WriteLog = line => {
    if (process.env.NODE_ENV === "development") {
        store.dispatch({type: "WriteLog", value: line})
        setTimeout(() => {
            store.dispatch({type: "RemoveFirstLog"})
        }, 1500)
    }
}

const mapStateToProps = (state) => ({
    lines: state.log.lines
});
export default connect(mapStateToProps)(Log)
