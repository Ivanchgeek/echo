import React from "react"
import "../../styles/SkyFall.sass"
import {connect} from "react-redux"

const pixelSize = 20
const margin = 2
const lineCount = Math.floor(document.documentElement.clientHeight / (pixelSize + margin))
const columnCount = Math.floor(document.documentElement.clientWidth / (pixelSize + margin))
const impulsLength = 8
const fps = 10
const generationTimeoutMin = 100
const generationTimeoutMax = 1000
const eggActivateKey = [115, 110, 97, 107, 101]
const initialSnakeLength = 10
const wallMaxLength = 15
const wallMinLength = 5

class SkyFall extends React.Component {

    constructor(props) {
        super(props)
        this.impulses = []
        this.generation = false
        this.keyPassed = 0
        this.snakeActive = false
        this.snake = {
            body: [],
            direction: 2
        }
        this.snakeScore = 0
        this.walls = []
        if (props.activeStartStatus) {
            this.props.dispatch({type: "StartLoading"})
        }
        if (this.props.tracked) {
            window.addEventListener("mousemove", this.trackPutter)
        }   
        if (this.props.egged) {
            window.addEventListener("keypress", this.keyboardManager)
        }
    }

    componentDidMount = () => {
        if (!this.snakeActive) {
            this.animateInterval = setInterval(() => {
                this.animateSkyFall()
            }, 1000 / fps)
            this.runImpulsGenerator()
        }
    }

    componentDidUpdate = () => {
        if (this.props.active && !this.generation) this.runImpulsGenerator()
    }

    renderDisplay = () => {
        var display = []
        for (let i = 0; i < lineCount; i++) {
            var line =[]
            for (let j = 0; j < columnCount; j++) {
                line.push(<div className="pixel" id={`pixel_${j}_${i}`} key={`${j}_${i}`}/>)
            }
            display.push(<div className="line" key={i}>{line}</div>)
        }
        return display
    }

    setPixel = (x, y, color, opacity) => {
        var pixel = document.getElementById(`pixel_${x}_${y}`)
        if (pixel !== null) {
            pixel.style.background = color
            pixel.style.opacity = opacity
        }
    }

    clear = () => {
        var pixels = document.getElementsByClassName("pixel")
        for (var i = 0; i < pixels.length; i++) {
            pixels[i].style.background = "#000000"
        }
    }

    drawImpuls = (x, y) => {
        var opacityStep = 1 / impulsLength
        for (var i = 0; i < impulsLength; i++) {
            this.setPixel(x, y - i, "#ffffff", opacityStep * (impulsLength - i) / 1.3)
        }
    }

    animateSkyFall = () => {
        this.clear()
        for (var i = 0; i < this.impulses.length; i++) {
            this.impulses[i].y++
            this.drawImpuls(this.impulses[i].x, this.impulses[i].y)
            if (this.impulses[i].y >= lineCount + impulsLength) this.impulses.splice(i, 1)
            if (this.impulses.length === 0 && this.props.awaitStop) {
                this.props.dispatch({type: "SetAwaitLoadingStop", value: false})
                this.props.dispatch({type: "HardStopLoading"})
                clearInterval(this.animateInterval)
            }
        }
    }

    runImpulsGenerator = () => {
        setTimeout(() => {
            this.generation = true
            this.impulses.push({x: Math.round(Math.random() * columnCount), y: 0})
            if (!this.props.awaitStop) this.runImpulsGenerator() 
            else this.generation = false
        }, generationTimeoutMin + Math.random() * (generationTimeoutMax - generationTimeoutMin))
    }

    trackPutter = event => {
        if (!this.snakeActive) {
            var candidates = document.elementsFromPoint(event.pageX, event.pageY)
            for (let i = 0; i < candidates.length; i++) {
                if (candidates[i].classList.contains("pixel")) {
                    candidates[i].animate([
                        {background: "#ffffff", opacity: .6},
                        {background: "#ffffff", opacity: 0}
                    ], {
                        duration: 300
                    })
                    break
                }
            }
        }
    }

    keyboardManager = event => {
        if (!this.snakeActive) {
            if (event.keyCode === eggActivateKey[this.keyPassed]) {
                this.keyPassed++
                if (this.keyPassed === eggActivateKey.length) {
                    this.snakeActive = true
                    this.props.dispatch({type: "StopLoading"})
                    setTimeout(this.initSnake, 1000 / fps * (lineCount + impulsLength))
                    console.log("good job")
                }
            }
        } else {
            switch (event.keyCode) {
                case 119:
                    if (this.snake.direction !== 2) this.snake.direction = 0
                    break
                case 100:
                    if (this.snake.direction !== 3) this.snake.direction = 1
                    break
                case 115:
                    if (this.snake.direction !== 0) this.snake.direction = 2
                    break
                case 97:
                    if (this.snake.direction !== 1) this.snake.direction = 3
                    break
                default:
                    break
            }
        }
    }

    initSnake = () => {
        this.addFood()
        var spawnPointAbsolute = document.getElementById("snake-swanpoint").getBoundingClientRect()
        var spawnPoint = {
            x: 0,
            y: 0
        }
        var offset = 0
        var spawnDetected = false
        while (!spawnDetected) {
            var spawnCandidates = document.elementsFromPoint(spawnPointAbsolute.x + spawnPointAbsolute.width / 2 + offset, spawnPointAbsolute.y + spawnPointAbsolute.height / 2 + offset)
            for (let i = 0; i < spawnCandidates.length; i++) {
                if (spawnCandidates[i].classList.contains("pixel")) {
                    var spawnPointRaw = spawnCandidates[i].id.split("_")
                    spawnPoint.x = parseInt(spawnPointRaw[1])
                    spawnPoint.y = parseInt(spawnPointRaw[2])
                    spawnDetected = true
                    break;
                }
            }
            if (!spawnDetected) {
                offset+=2
                if (offset >= pixelSize) {
                    console.error("error to find snake spawnPoint");
                    break;
                }
            }
        }
        this.snake.body[0] = spawnPoint
        var snakeGrow = setInterval(() => {
            this.addToSnake()
            if (this.snake.body.length >= initialSnakeLength) {
                clearInterval(snakeGrow)
            }
        }, 150)
        setTimeout(() => {
            this.props.dispatch({type: "HideLogo"})
            this.generateWalls()
        }, 1000)
        this.snakeLifecycleInterval = setInterval(this.snakeLifecycle, 150)
    }

    snakeLifecycle = () => {
        this.moveSnake()
        if (this.snakeAlive()) {
            this.clear()
            this.setPixel(this.food.x, this.food.y, "#7ba832", .8)
            for (let i = 0; i < this.walls.length; i++) {
                this.setPixel(this.walls[i].x, this.walls[i].y, "#ffffff", 1)
            }
            this.drawSnake()
            if (this.food.x === this.snake.body[0].x && this.food.y === this.snake.body[0].y) {
                this.addToSnake()
                this.addFood()
            }
        } else {
            clearInterval(this.snakeLifecycleInterval)
        }
    }

    addToSnake = () => {
        var add = {
            x: this.snake.body[0].x,
            y: this.snake.body[0].y
        }
        switch (this.snake.direction) {
            case 0:
                add.y--
                break
            case 1:
                add.x++
                break
            case 2:
                add.y++
                break
            case 3:
                add.x--
                break
            default:
                console.error("wrong direction")
                return
        }
        this.snake.body.unshift(add)
        this.snakeScore = this.snake.body.length - initialSnakeLength
        if (this.snakeScore >= 0) this.refs.score.innerHTML = this.snakeScore
    }

    drawSnake = () => {
        for (let i = 0; i < this.snake.body.length; i++) {
            this.setPixel(this.snake.body[i].x, this.snake.body[i].y, "#ffffff", .6)
        }
    }

    moveSnake = () => {
        this.snake.body.pop()
        this.addToSnake()
    }

    snakeAlive = () => {
        for (let i = 0; i < this.snake.body.length - 1; i++) {
            for (let j = i + 1; j < this.snake.body.length; j++) {
                if (this.snake.body[i].x === this.snake.body[j].x && this.snake.body[i].y === this.snake.body[j].y) {
                    return false
                }
            }
        }
        var pixel = document.getElementById(`pixel_${this.snake.body[0].x}_${this.snake.body[0].y}`)
        if (pixel === null || pixel.dataset.lava === "1") return false
        return true
    }

    addFood = () => {
        this.food = {
            x: Math.floor(Math.random() * columnCount),
            y: Math.floor(Math.random() * lineCount)
        }
        var pixel = document.getElementById(`pixel_${this.food.x}_${this.food.y}`)
        if (pixel === null || pixel.dataset.lava === "1") this.addFood()
    }

    generateWalls = () => {
        this.walls = []
        for (let i = 0; i < 10; i++) {
            var length = Math.round(Math.random() * (wallMaxLength - wallMinLength) + wallMinLength)
            var x = Math.round(Math.random() * (columnCount - wallMaxLength)) 
            var y = Math.round(Math.random() * (lineCount - wallMaxLength))
            var vertical = Math.random() > 0.5
            for (let j = 0; j < length; j++) {
                if (vertical) {
                    this.walls.push({x: x, y: y + j})
                    document.getElementById(`pixel_${x}_${y + j}`).dataset.lava = "1"
                } else {
                    this.walls.push({x: x + j, y: y})
                    document.getElementById(`pixel_${x + j}_${y}`).dataset.lava = "1"
                }
            } 
        }
    }

    render() {
        return (
            <div className={`skyfall ${this.props.active || this.snakeActive ? "active" : ""}`}>
                <div className="snakeScore" ref="score"/>
                {this.renderDisplay()}
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    active: state.transitions.loading,
    awaitStop: state.transitions.awaitLoadingStop
});
export default connect(mapStateToProps)(SkyFall)