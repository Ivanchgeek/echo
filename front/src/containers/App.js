import React from 'react';
import {BrowserView, MobileView} from "react-device-detect";
import Desktop from "./Desktop"
import Mobile from "./Mobile"
import "../styles/App.sass"

class App extends React.Component {

    render() {
        return (
            <>
              <BrowserView><Desktop/></BrowserView>
              <MobileView><Mobile/></MobileView>
            </>
        );
    }
}

export default App;
