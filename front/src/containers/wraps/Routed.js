import React from "react"
import {Route, Switch, Router } from 'react-router-dom'
import {CSSTransition, TransitionGroup} from "react-transition-group"
import {connect} from "react-redux"
import history from "../../history/history"
import Home from "../views/Home"
import SkyFall from "../components/SkyFall"
import Faq from "../views/Faq"
import Game from "../views/Game"
import Result from "../views/Result"
import Notifications from "../components/Notifications"
import Fullscreen from "../components/Fullscreen"
import ShareDom from "../../utl/share"
import Log from "../components/Log"

class Routed extends React.Component {
    render() {
        return (
            <Router history={history}>
                <Route render={({location}) => {
                    return (
                        <TransitionGroup className="mobile">
                            <CSSTransition
                                key={location.pathname}
                                timeout={{ enter: 1000, exit: 1000 }}
                                classNames={"view"}>
                                    <Switch location={location}>
                                        <Route exact path="/" component={Home}/>
                                        <Route exact path="/faq" component={Faq}/>
                                        <Route exact path="/game" component={Game}/>
                                        <Route exact path="/result" component={Result}/>
                                    </Switch>
                            </CSSTransition>
                            <SkyFall activeStartStatus={false} ref={this.skyfall}/>
                            <Notifications/>
                            <Fullscreen/>
                            <ShareDom/>
                            <Log/>
                        </TransitionGroup>
                    )
                }}/>
            </Router>
        )
    }
}

const mapStateToProps = (state) => ({
});
export default connect(mapStateToProps)(Routed)
