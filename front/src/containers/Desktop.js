import React from "react"
import Logo from "./components/Logo"
import SkyFall from "./components/SkyFall"
import {connect} from "react-redux"

class Desktop extends React.Component {

    componentDidMount = () => {
        this.refs.logo.animate()
    }

    render() {
        return (
            <div className="desktop">
                <div className="content" style={{opacity: this.props.logoVisible ? "1" : "0"}}>
                    <Logo ref="logo" width="15vw" height="15vw"/>
                    <h1>Get back to us from your smartphone :)</h1>
                </div>
                <SkyFall activeStartStatus={true} tracked egged/>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    logoVisible: state.transitions.logoVisible
});
export default connect(mapStateToProps)(Desktop)