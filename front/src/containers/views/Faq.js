import React from "react"
import {connect} from "react-redux"
import RotateGameplay from "../components/RotateGameplay"
import BulletGameplay from "../components/BulletGameplay"
import RecoilGameplay from "../components/RecoilGameplay"
import Logo from "../components/Logo"
import cross from "../../stuff/cross.svg"
import history from "../../history/history"
import {share} from "../../utl/share"

class Faq extends React.Component {

    componentDidMount = () => {
        this.props.dispatch({type: "StopLoading"})
        this.refs.logo.animate()
    }

    close = () => {
        this.props.dispatch({type: "StartLoading"})
        history.push({pathname: "/"})
    }

    share = () => {
        share({
            url: `Echo game (check it out) ${window.location.origin}`,
            title: "",
            text: ""
        })
    }

    render() {
        return(
            <div className="faqview">
                <img className="close" src={cross} alt="close cross" onClick={this.close}/>
                <Logo width="40vw" height="40vw" ref="logo"/>
                <h2>Minimalistik online game</h2>
                <div className="section">GAMEPLAY</div>
                <div className="point">Tap and move to rorate</div>
                <RotateGameplay/>
                <div className="point">Beware of bullets</div>
                <BulletGameplay/>
                <div className="point">Use recoil to move</div>
                <RecoilGameplay/>
                <div className="point">With each round, the game will become more difficult, becasuse more opponents who copy your past actions would appear</div>
                <div className="section">LINKS</div>
                <h2 className="link">Desingned and powered by <a target="_blank" href="https://t.me/ivanchgeek" rel="noopener noreferrer">@ivanchgeek</a></h2>
                <h2 className="link">Assistance in soundtrack selection <a target="_blank" href="https://www.instagram.com/nchka/" rel="noopener noreferrer">@nchka</a></h2>
                <h2 className="link">Help with architecture <a target="_blank" href="https://t.me/deniskamazur" rel="noopener noreferrer">@deniskamazur</a></h2>
                <h2 className="link">Idea from $unknown_user from Junction</h2>
                <h2 className="link"><a target="_blank" href="https://gitlab.com/Ivanchgeek/echo" rel="noopener noreferrer">Project’s git<br/>page</a></h2>
                <hr></hr>
                <div className="enjoy">Enjoy the game</div>
                <button className="share" onClick={this.share}>SHARE</button>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
});
export default connect(mapStateToProps)(Faq)