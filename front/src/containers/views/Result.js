import React from "react"
import newGame from "../../stuff/new.svg"
import history from "../../history/history"
import {connect} from "react-redux"

class Result extends React.Component {

    constructor (props) {
        super(props)
        this.state = {
            label: ""
        }
        switch(new URLSearchParams(window.location.search).get("status")) {
            case "win":
                this.state = {
                    label: "You win"
                }
                break
            case "lose":
                this.state = {
                    label: "You lose"
                }
                break
            default:
                this.state = {
                    label: "You are nobody"
                }
        }
    }

    componentDidMount = () => {
        this.props.dispatch({type: "StopLoading"})
    }

    startNew = () => {
        this.props.dispatch({type: "StartLoading"})
        history.push({pathname: "/"})
    }

    render() {
        return (
            <div className="resultview">
                <div className="label">{this.state.label}</div>
                <img src={newGame} alt="new game" onClick={this.startNew} ref="new" className="new"></img>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
});
export default connect(mapStateToProps)(Result)