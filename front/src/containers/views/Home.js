import React from "react"
import Logo from "../components/Logo"
import history from "../../history/history"
import {connect} from "react-redux"
import {toggleFullScreen} from "../components/Fullscreen"

class Home extends React.Component {

    componentDidMount = () => {
        this.refs.logo.animate()
        this.props.dispatch({type: "StopLoading"})
    }

    prepareGame = () => {
        toggleFullScreen()
        history.push({pathname: "/game"})
    }

    goToFaq = event => {
        event.stopPropagation()
        this.props.dispatch({type: "StartLoading"})
        history.push({pathname: "/faq"})
    }

    render() {
        return(
            <div className="home" onClick={this.prepareGame}>
                <div className="content">
                    <Logo ref="logo" width="60vw" height="60vw"/>
                    <h1>Tap to start</h1>
                    <p>fullscreen mode required</p>
                </div>
                <p className="sound">sound on</p>
                <h1 className="faq" onClick={this.goToFaq}>i</h1>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
});
export default connect(mapStateToProps)(Home)