import React from "react"
import {connect} from "react-redux"
import {InitLobby, JoinLobby} from "../../actions/networking"
import {share} from "../../utl/share"
import Joystick from "../components/Joystick"
import {SyncToRemote, CallRemoteImitation, PushStateHash, RequetStateHash} from "../../actions/networking"
import {hash} from "hash-it"

const fps = 30
const playerSize = document.documentElement.clientWidth / 8
const markerSize = playerSize / 5
const shootHoleSize = 6
const shootHoleBorderLength = (playerSize - shootHoleSize) * 4
const bulletSize = shootHoleBorderLength / 12
const bulletWeight = 10
const playerWeight = 25
const shootImpuls = 200
const bulletFriction = 2
const playerFriction = .5
const g = 9.8
const bulletDeltaImpuls = bulletWeight * g * bulletFriction / fps
const playerDeltaImpuls = playerWeight * g * playerFriction / fps
const syncTimeout = 100
const coolDown = 1000
const bulletTransition = 150
const playerTransition = 500
const lastRound = 2

class Game extends React.Component {

    constructor(props) {
        super(props)
        this.join = new URLSearchParams(window.location.search).get("join") || "init"
        this.state = {
            qrLoaded: false
        }
        this.gameIterater = undefined
        this.lastSync = - syncTimeout
        this.stateHashRecording = false
        this.hashHistory = new Map()
        this.gameState = {
            players: [
                {
                    x: (document.documentElement.clientWidth - playerSize) / 2,
                    y: document.documentElement.clientHeight -  3 * playerSize,
                    marker: "#4431B8",
                    angle: 0,
                    impuls: {
                        x: 0,
                        y: 0
                    },
                    shootTimeStamp: - coolDown,
                    alive: true,
                    motionPattern: [],
                    initBuffer: {
                        x: (document.documentElement.clientWidth - playerSize) / 2,
                        y: document.documentElement.clientHeight -  3 * playerSize,
                        angle: 0
                    }
                },
                {
                    x: (document.documentElement.clientWidth - playerSize) / 2,
                    y: 2 * playerSize,
                    marker: "#CF3131",
                    angle: 180,
                    impuls: {
                        x: 0,
                        y: 0
                    },
                    shootTimeStamp: - coolDown,
                    alive: true,
                    motionPattern: [],
                    initBuffer: {
                        x: (document.documentElement.clientWidth - playerSize) / 2,
                        y: 2 * playerSize,
                        angle: 180
                    }
                }
            ],
            bullets: [],
            lobbyScore: 0,
            joinScore: 0,
            timer: 0,
            prepairingNextRound: false,
            round: 0
        }
        this.awaitFullscreen = false
        if (this.join === "init") {
            this.me = 0
            this.partner = 1
            this.props.dispatch(InitLobby())
        } else {
            this.me = 1
            this.partner = 0
            if (this.props.fullscreen) this.props.dispatch(JoinLobby(this.join))
            else this.awaitFullscreen = true
        }
    }

    share = event => {
        event.stopPropagation()
        share({
            url: `${window.location.origin}/game?join=${this.props.LobbyToken}`,
            title: "Echo",
            text: "Let's play echo together"
        })
    }

    componentDidUpdate = () => {
        if (this.props.fullscreen && this.awaitFullscreen) {
            this.awaitFullscreen = false
            this.props.dispatch(JoinLobby(this.join))
        }
        if (this.props.ready && this.gameIterater === undefined) {
            this.gameIterater = setInterval(this.gameLoop, 1000 / fps)
            if (this.join === "init") CallRemoteImitation(document.documentElement.clientWidth, document.documentElement.clientHeight)
        }
        if (this.props.update) {
            this.props.dispatch({type: "SubmitUpdate"})
            this.executeCmd(this.props.updatePack)
        }
        if (this.props.syncUpdate) {
            this.props.dispatch({type: "SubmitSyncUpdate"})
            var pack = this.props.syncPack
            switch (pack.type) {
                case "hashRequest":
                    PushStateHash(this.gameState)
                    break
                case "pushHash":
                    if (pack.blocked) break
                    var start = -1
                    var finish = 0
                    var sync = () => {
                        SyncToRemote(this.gameState)
                        this.hashHistory = new Map()
                    }
                    this.stateHashRecording = false
                    this.hashHistory.forEach((hash, timestamp) => {
                        if (timestamp < start && start !== -1) {
                            start = timestamp
                        }
                        if (timestamp > finish) {
                            finish = timestamp
                        }
                    })
                    if (start > pack.timestamp) {
                        sync()
                        break
                    }
                    if (finish < pack.timestamp) {
                        sync()
                        break
                    }
                    if (this.hashHistory.get(pack.timestamp) !== undefined) {
                        if (this.hashHistory.get(pack.timestamp) !== pack.hash) {
                            sync()
                            break
                        }
                    } else {
                        sync()
                    }
                    break
                default:
                    console.log("bad sync pack type")
                    break
            }
        }
        
    }

    executeCmd = cmd => {
        var target
        if (cmd.target === "remote") target = this.partner
        if (cmd.target === "self" && target === undefined) target = this.me
        try {
            if (target === undefined) target = parseInt(cmd.target)
        } catch(e) {
            console.error("bad cmd format")
            return
        }
        if (target <= 1) {
            this.gameState.players[target].motionPattern.push({
                cmd: cmd,
                timestamp: this.gameState.timer
            })
        }
        switch (cmd.type) {
            case "rotation":
                if (this.gameState.players[target].blackMark !== undefined) break
                this.gameState.players[target].angle = cmd.angle
                break
            case "shoot":
                var shooter = this.gameState.players[target]
                if (this.gameState.timer - shooter.shootTimeStamp >= coolDown && shooter.blackMark === undefined) { 
                    var x = shooter.x + playerSize / 2 - bulletSize / 2
                    var y = shooter.y + playerSize / 2  - bulletSize / 2
                    this.gameState.bullets.push({
                        x: x, 
                        y: y, 
                        angle: this.gameState.players[target].angle,
                        opacity: 1,
                        stopTimeStamp: 0,
                        impuls: {
                            x: this.gameState.players[target].impuls.x / playerWeight * bulletWeight + shootImpuls * Math.sin(shooter.angle * Math.PI / 180),
                            y: this.gameState.players[target].impuls.y / playerWeight * bulletWeight - shootImpuls * Math.cos(shooter.angle * Math.PI / 180)
                        },
                        active: false,
                        owner: target
                    })
                    this.gameState.players[target].impuls.x -= shootImpuls * Math.sin(shooter.angle * Math.PI / 180)
                    this.gameState.players[target].impuls.y += shootImpuls * Math.cos(shooter.angle * Math.PI / 180)
                    this.gameState.players[target].shootTimeStamp = this.gameState.timer
                }
                break
            case "sync":
                this.gameState = cmd.state
                break
            case "imitation":
                var stage = this.refs.stage
                if (stage !== undefined) {
                    if (cmd.height > document.documentElement.clientHeight) {
                        CallRemoteImitation(document.documentElement.clientWidth, document.documentElement.clientHeight)
                        break
                    }
                    stage.width = cmd.width
                    stage.height = cmd.height
                    if (cmd.height / cmd.width !== document.documentElement.clientHeight / document.documentElement.clientWidth) {
                        let height = document.documentElement.clientWidth * cmd.height / cmd.width
                        stage.style.height = `${height}px`
                        stage.style.top = `${(document.documentElement.clientHeight - height)/2}px`
                    }
                }
                break
            default:
                console.log("bad cmd type")
                break
        }
    }

    gameLoop = () => {
        var stage = this.refs.stage
        if (stage !== undefined) {
            var ctx = stage.getContext("2d")
            this.gameState.timer += 1000 / fps
            this.botCommander()
            this.manageBulletsLifeCycle()
            if (!this.gameState.prepairingNextRound) this.applyPhysics()
            if (this.join === "init" && Date.now() - this.lastSync >= syncTimeout) {
                if (this.props.blockersCount <= 0) {
                    this.lastSync = Date.now()
                    this.stateHashRecording = true
                    RequetStateHash()
                } else {
                    this.lastSync = Date.now() + this.props.BlockerTimeout
                }
            }
            if (this.stateHashRecording) {
                this.hashHistory.set(this.gameState.timer, hash(this.gameState))
            }
            ctx.clearRect(0, 0, stage.width, stage.height)
            ctx.strokeStyle = "rgba(255, 255, 255, 0.5)"
            ctx.strokeRect(0, 0, stage.width, stage.height)
            for (let i = 0; i < this.gameState.bullets.length; i++) {
                var bullet = this.gameState.bullets[i]
                this.drawBullet(ctx, bullet.x, bullet.y, bullet.angle, bullet.opacity)
            }
            for (let i = 0; i < this.gameState.players.length; i++) {
                var player = this.gameState.players[i]
                if (player.alive) {
                    if (this.gameState.timer - player.blackMark > playerTransition) {
                        player.alive = false
                        continue
                    }
                    this.drawPlayer(ctx, player.x, player.y, player.angle, player.marker, player.shootTimeStamp, player.blackMark === undefined ? 0 : (this.gameState.timer - player.blackMark) / playerTransition)
                }
            }
        } else {
            clearInterval(this.gameIterater)
        }
    }

    drawPlayer = (ctx, x, y, angle, marker, shootTimeStamp, deadProgress) => {
        ctx.save()
        ctx.translate(x + playerSize / 2, y + playerSize / 2)
        ctx.rotate(angle * Math.PI / 180)
        ctx.fillStyle = "white"
        if (deadProgress !== 0) {
            if (deadProgress > .5) {
                ctx.fillStyle = "transparent"
            } else {
                ctx.fillStyle = `rgba(255, 255, 255, ${1 - deadProgress * 2})`
            }
        }
        ctx.fillRect(-playerSize / 2, -playerSize / 2, playerSize, playerSize)
        ctx.strokeStyle = "white"
        if (deadProgress !== 0) {
            ctx.setLineDash([playerSize * 4 * deadProgress / 5, playerSize * 4 * (1 - deadProgress) / 5])
        }
        ctx.strokeRect(-playerSize / 2, -playerSize / 2, playerSize, playerSize)
        ctx.strokeStyle = marker
        ctx.lineWidth = 2
        var coolDownCofficient = (this.gameState.timer - shootTimeStamp) / coolDown
        if (this.gameState.timer - shootTimeStamp >= coolDown) coolDownCofficient = 1
        ctx.setLineDash([markerSize * 4 * coolDownCofficient, markerSize * 4 * (1 - coolDownCofficient)])
        ctx.strokeRect(-markerSize / 2, -markerSize / 2, markerSize, markerSize)
        ctx.lineWidth = shootHoleSize
        ctx.setLineDash([0, shootHoleBorderLength / 12, shootHoleBorderLength / 12, shootHoleBorderLength / 4 * 3 + shootHoleBorderLength / 12])
        ctx.strokeRect(-(playerSize - shootHoleSize) / 2, -(playerSize - shootHoleSize) / 2, playerSize - shootHoleSize / 2, playerSize - shootHoleSize / 2)
        ctx.restore()
    }

    drawBullet = (ctx, x, y, angle, opacity) => {
        ctx.save()
        ctx.translate(x + bulletSize / 2, y + bulletSize / 2)
        ctx.rotate(angle * Math.PI / 180)
        ctx.fillStyle = `rgba(255, 255, 255, ${opacity})`
        ctx.fillRect(-bulletSize / 2, -bulletSize/ 2, bulletSize, bulletSize)
        ctx.restore()
    }

    applyPhysics = () => {
        if (!this.gameState.prepairingNextRound) this.applyMotion()
        if (!this.gameState.prepairingNextRound) this.applyColisions()
    }

    applyMotion = () => {
        for (let i = 0; i < this.gameState.bullets.length; i++) {
            this.gameState.bullets[i].x += this.gameState.bullets[i].impuls.x / bulletWeight
            this.gameState.bullets[i].y += this.gameState.bullets[i].impuls.y / bulletWeight
            var bulletImpulsHypotenuse = Math.sqrt(Math.pow(this.gameState.bullets[i].impuls.x, 2) + Math.pow(this.gameState.bullets[i].impuls.y, 2))
            var bulletDeltaImpulsX = bulletDeltaImpuls * Math.abs(this.gameState.bullets[i].impuls.x) / bulletImpulsHypotenuse
            var bulletDeltaImpulsY = bulletDeltaImpuls * Math.abs(this.gameState.bullets[i].impuls.y) / bulletImpulsHypotenuse
            if (Math.abs(this.gameState.bullets[i].impuls.x) < bulletDeltaImpulsX) {
                this.gameState.bullets[i].impuls.x = 0
            }
            if (this.gameState.bullets[i].impuls.x > 0) {
                this.gameState.bullets[i].impuls.x -= bulletDeltaImpulsX
            }
            if (this.gameState.bullets[i].impuls.x < 0) {
                this.gameState.bullets[i].impuls.x += bulletDeltaImpulsX
            }
            if (Math.abs(this.gameState.bullets[i].impuls.y) < bulletDeltaImpulsY) {
                this.gameState.bullets[i].impuls.y = 0
            }
            if (this.gameState.bullets[i].impuls.y > 0) {
                this.gameState.bullets[i].impuls.y -= bulletDeltaImpulsY
            }
            if (this.gameState.bullets[i].impuls.y < 0) {
                this.gameState.bullets[i].impuls.y += bulletDeltaImpulsY
            }
        }
        for (let i = 0; i < this.gameState.players.length; i++) {
            this.gameState.players[i].x += this.gameState.players[i].impuls.x / playerWeight
            this.gameState.players[i].y += this.gameState.players[i].impuls.y / playerWeight
            var playerImpulsHypotenuse = Math.sqrt(Math.pow(this.gameState.players[i].impuls.x, 2) + Math.pow(this.gameState.players[i].impuls.y, 2))
            var playerDeltaImpulsX = playerDeltaImpuls * Math.abs(this.gameState.players[i].impuls.x) / playerImpulsHypotenuse
            var playerDeltaImpulsY = playerDeltaImpuls * Math.abs(this.gameState.players[i].impuls.y) / playerImpulsHypotenuse
            if (Math.abs(this.gameState.players[i].impuls.x) < playerDeltaImpulsX) {
                this.gameState.players[i].impuls.x = 0
            }
            if (this.gameState.players[i].impuls.x > 0) {
                this.gameState.players[i].impuls.x -= playerDeltaImpulsX
            }
            if (this.gameState.players[i].impuls.x < 0) {
                this.gameState.players[i].impuls.x += playerDeltaImpulsX
            }
            if (Math.abs(this.gameState.players[i].impuls.y) < playerDeltaImpulsY) {
                this.gameState.players[i].impuls.y = 0
            }
            if (this.gameState.players[i].impuls.y > 0) {
                this.gameState.players[i].impuls.y -= playerDeltaImpulsY
            }
            if (this.gameState.players[i].impuls.y < 0) {
                this.gameState.players[i].impuls.y += playerDeltaImpulsY
            }
        }
    }

    applyColisions = () => {
        var stage = this.refs.stage
        var colision
        for (let i = 0; i < this.gameState.players.length; i++) {
            colision = this.getSquareColision(this.gameState.players[i], playerSize)
            for (let j = 0; j < colision.length; j++) {
                if ((colision[j].y < 0 && this.gameState.players[i].impuls.y < 0) || (colision[j].y > stage.height && this.gameState.players[i].impuls.y > 0)) {
                    this.gameState.players[i].impuls.y = -this.gameState.players[i].impuls.y
                }
                if ((colision[j].x < 0 && this.gameState.players[i].impuls.x < 0) || (colision[j].x > stage.width && this.gameState.players[i].impuls.x > 0)) {
                    this.gameState.players[i].impuls.x = -this.gameState.players[i].impuls.x
                }
            }
        }
        for (let i = 0; i < this.gameState.bullets.length; i++) {
            colision = this.getSquareColision(this.gameState.bullets[i], bulletSize)
            for (let j = 0; j < colision.length; j++) {
                if ((colision[j].y < 0 && this.gameState.bullets[i].impuls.y < 0) || (colision[j].y > stage.height && this.gameState.bullets[i].impuls.y > 0)) {
                    this.gameState.bullets[i].impuls.y = -this.gameState.bullets[i].impuls.y
                }
                if ((colision[j].x < 0 && this.gameState.bullets[i].impuls.x < 0) || (colision[j].x > stage.width && this.gameState.bullets[i].impuls.x > 0)) {
                    this.gameState.bullets[i].impuls.x = -this.gameState.bullets[i].impuls.x
                }
            }
        }
        for (let i = 0; i < this.gameState.bullets.length; i++) {
            for (let j = 0; j < this.gameState.players.length; j++) {
                if (!this.gameState.prepairingNextRound) {
                    if (this.getObjectsHitted(this.gameState.bullets[i], bulletSize, this.gameState.players[j], playerSize)) {
                        if (this.gameState.bullets[i].active && this.gameState.players[j].blackMark === undefined) {
                            this.gameState.players[j].blackMark = this.gameState.timer
                            if (j === 0) {
                                this.gameState.joinScore++
                                this.NextRound()
                            }
                            if (j === 1) {
                                this.gameState.lobbyScore++
                                this.NextRound()
                            }
                        }
                    } else {
                        if (this.gameState.bullets[i] !== undefined && !this.gameState.bullets[i].active && this.gameState.bullets[i].owner === j) {
                            this.gameState.bullets[i].active = true
                        }
                    }
                }
            }
        }
    }

    getSquareColision = (object, size) => {
        try {
            var angle = object.angle % 90
            var baseX = object.x + size * angle / 90
            var baseY = -Math.sqrt(Math.pow(size, 2) / 2 - Math.pow((size * (angle / 90 - 0.5)), 2)) + object.y + size / 2
            return [
                {
                    x: baseX, 
                    y: baseY
                }, 
                {
                    x: baseX + size * Math.cos(angle * Math.PI / 180), 
                    y: baseY + size * Math.sin(angle * Math.PI / 180)
                }, 
                {
                    x: object.x + size - (baseX - object.x), 
                    y: object.y + size + (object.y - baseY)
                }, 
                {
                    x: object.x - (baseX + size * Math.cos(angle * Math.PI / 180) - (object.x + size)),
                    y: object.y + size - (baseY + size * Math.sin(angle * Math.PI / 180) - object.y)
                }
            ]
        } catch(e) {
            return [{x: 0, y: 0}, {x: 0, y: 0}, {x: 0, y: 0}, {x: 0, y: 0}]
        }
    }

    getObjectsHitted = (object, objectSize, subject, subjectSize) => {
        var objectColision = this.getSquareColision(object, objectSize)
        var subjectColision = this.getSquareColision(subject, subjectSize)
        for (let i = 0; i < subjectColision.length; i++) {
            if (this.getPointInColision(objectColision[i], subjectColision)) return true
        }
        return false
    }

    getPointInColision = (point, colision) => {
        var sides = []
        for (let i = 0; i < 4; i++) {
            let side = {}
            let point_1 = colision[i]
            let point_2 = colision[i === 3 ? 0 : i + 1]
            side.range = {}
            side.range.includes = i % 2 === 0
            if (point_1.x === point_2.x) {
                side.xEquality = true
                side.b = point_1.x
                side.k = 1
                if (point_1.y < point_2.y) {
                    side.range.start = point_1.y
                    side.range.end = point_2.y
                } else {
                    side.range.start = point_2.y
                    side.range.end = point_1.y
                }
                sides.push(side)
                continue
            }
            side.xEquality = false
            side.k = (point_2.y - point_1.y) / (point_2.x - point_1.x)
            side.b = point_1.y - side.k * point_1.x
            if (point_1.x < point_2.x) {
                side.range.start = point_1.x
                side.range.end = point_2.x
            } else {
                side.range.start = point_2.x
                side.range.end = point_1.x
            }
            sides.push(side)
        }
        var basePoint = {}
        basePoint.x = sides[0].range.start + (sides[0].range.end - sides[0].range.start) / 2
        if (basePoint.x === point.x) {
            basePoint.x += (sides[0].range.end - sides[0].range.start) * 0.1
        }
        basePoint.y = basePoint.x * sides[0].k + sides[0].b
    
        var controlLine = {}
        controlLine.k = (basePoint.y - point.y) / (basePoint.x - point.x)
        controlLine.b = point.y - point.x * controlLine.k
        controlLine.incremental = basePoint.x > point.x
    
        var intersections = 0
        for (let i = 0; i < 4; i++) {
            if (sides[i].xEquality) {
                let y = controlLine.k * sides[i].b + controlLine.b
                if (((y > sides[i].range.start && y < sides[i].range.end) || (sides[i].range.includes && (y === sides[i].range.start || y === sides[i].range.end))) && ((controlLine.incremental && sides[i].b > point.x) || (!controlLine.incremental && sides[i].b < point.x))) {
                    intersections++
                }
            } else {
                let x = (sides[i].b - controlLine.b) / (controlLine.k - sides[i].k)
                if (((x < sides[i].range.end && x > sides[i].range.start) || (sides[i].range.includes && (x === sides[i].range.start || x === sides[i].range.end))) && ((controlLine.incremental && x > point.x) || (!controlLine.incremental && x < point.x))) {
                    intersections++
                }
            }
        }

        return intersections % 2 === 1
    }

    manageBulletsLifeCycle = () => {
        var  bulletsUpdate = []
        for (let i = 0; i < this.gameState.bullets.length; i++) {
            if (this.gameState.bullets[i].impuls.x === 0 && this.gameState.bullets[i].impuls.y === 0 && this.gameState.bullets[i].stopTimeStamp === 0) {
                this.gameState.bullets[i].stopTimeStamp = this.gameState.timer
            }
            if (this.gameState.bullets[i].stopTimeStamp !== 0) {
                if (this.gameState.timer - this.gameState.bullets[i].stopTimeStamp < bulletTransition) {
                    this.gameState.bullets[i].opacity = 1 - (this.gameState.timer - this.gameState.bullets[i].stopTimeStamp) / bulletTransition
                } else {
                    continue
                }
            }
            bulletsUpdate.push(this.gameState.bullets[i])
        }
        this.gameState.bullets = bulletsUpdate
    }

    stopBullets = () => {
        for (let i = 0; i < this.gameState.bullets.length; i++) {
            this.gameState.bullets[i].impuls.x = 0
            this.gameState.bullets[i].impuls.y = 0
        }
    }

    stopPlayers = () => {
        for (let i = 0; i < this.gameState.players.length; i++) {
            this.gameState.players[i].impuls.x = 0
            this.gameState.players[i].impuls.y = 0
        }
    }

    NextRound = () => {
        this.gameState.prepairingNextRound = true
        this.stopBullets()
        this.stopPlayers()
        if (this.gameState.round === lastRound) {
            this.showResults()
            return
        }
        if (this.join === "init") {
            setTimeout(this.processNextRound, playerTransition)
        }
    }

    processNextRound = () => {
        this.gameState.round++
        this.gameState.timer = 0
        var firstPosition
        var secondPosition
        this.gameState.bullets = []
        for (let i = 0; i < this.gameState.players.length; i++) {
            this.gameState.players[i].x = this.gameState.players[i].initBuffer.x
            this.gameState.players[i].y = this.gameState.players[i].initBuffer.y
            this.gameState.players[i].angle = this.gameState.players[i].initBuffer.angle
            this.gameState.players[i].shootTimeStamp = - coolDown
            this.gameState.players[i].blackMark = undefined
            this.gameState.players[i].alive = true
            this.gameState.players[i].marker = "#C5C5C5"
            for (let j = 0; j < this.gameState.players[i].motionPattern.length; j++) {
                this.gameState.players[i].motionPattern[j].cmd.target = (i + 2).toString()
            }
        }
        switch (this.gameState.round) {
            case 1:
                firstPosition = {
                    x: document.documentElement.clientWidth / 3 - playerSize / 2,
                    y: document.documentElement.clientHeight -  2 * playerSize
                }
                secondPosition = {
                    x: document.documentElement.clientWidth / 3 - playerSize / 2,
                    y: playerSize
                }
                break
            case 2:
                firstPosition = {
                    x: document.documentElement.clientWidth / 3 * 2 - playerSize / 2,
                    y: document.documentElement.clientHeight -  2 * playerSize
                }
                secondPosition = {
                    x: document.documentElement.clientWidth / 3 * 2 - playerSize / 2,
                    y: playerSize
                }
                break
            default:
                this.props.dispatch({type: "Notify", value: "Round error."})
                return
        }
        var playerOne = {
            x: firstPosition.x,
            y: firstPosition.y,
            marker: "#4431B8",
            angle: 0,
            impuls: {
                x: 0,
                y: 0
            },
            shootTimeStamp: - coolDown,
            alive: true,
            motionPattern: [],
            initBuffer: {
                x: firstPosition.x,
                y: firstPosition.y,
                angle: 0
            }
        }
        var playerTwo = {
            x: secondPosition.x,
            y: secondPosition.y,
            marker: "#CF3131",
            angle: 180,
            impuls: {
                x: 0,
                y: 0
            },
            shootTimeStamp: - coolDown,
            alive: true,
            motionPattern: [],
            initBuffer: {
                x: secondPosition.x,
                y: secondPosition.y,
                angle: 180
            }
        }
        this.gameState.players.unshift(playerTwo)
        this.gameState.players.unshift(playerOne)
        this.gameState.prepairingNextRound = false
        SyncToRemote(this.gameState)
    }

    botCommander = () => {
        for (let i = 2; i < this.gameState.players.length; i++) {
            for (let j = 0; j < this.gameState.players[i].motionPattern.length; j++) {
                if (this.gameState.players[i].motionPattern[j].timestamp === this.gameState.timer) {
                    this.executeCmd(this.gameState.players[i].motionPattern[j].cmd)
                }
            }
        }
    }

    showResults = () => {
        this.props.dispatch({type: "StartLoading"})
        clearInterval(this.gameIterater)
        if (this.join === "init") {
            window.location = `/result?status=${this.gameState.lobbyScore > this.gameState.joinScore ? "win" : "lose"}`
            return
        }
        window.location = `/result?status=${this.gameState.lobbyScore < this.gameState.joinScore ? "win" : "lose"}`
    }

    render() {
        const {LobbyToken, impulsing, prepairing, ready} = this.props
        const {qrLoaded} = this.state
        return (
            <div className="game">
                <div className={`invite ${!impulsing && qrLoaded && !prepairing && !ready ? "active" : ""}`}>
                    <p>Your friend should scan the qr-code or follow the link to play with you.</p>
                    <img alt="qrcode" src={`https://api.qrserver.com/v1/create-qr-code/?size=200x200&data=${window.location.origin}/game?join=${LobbyToken}`} 
                        onLoad={() => {this.setState({qrLoaded: true})}}/>
                    <button onClick={this.share}>Share link</button>
                </div>
                <p className={`score ${ready ? "active" : ""}`}>
                    <span className="lobby">{this.gameState.lobbyScore}</span>
                    /
                    <span className="join">{this.gameState.joinScore}</span>
                </p>
                <canvas className={`stage ${ready ? "active" : ""} ${this.join !== "init" ? "mirror" : ""}`} ref="stage" id="stage" 
                    width={document.documentElement.clientWidth} height={document.documentElement.clientHeight}/>
                <Joystick ready={ready} active={ready}/>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    LobbyToken: state.networking.LobbyToken,
    impulsing: state.game.impulsing,
    prepairing: state.game.prepairing,
    ready: state.game.ready,
    update: state.game.update,
    updatePack: state.game.updatePack,
    fullscreen: state.fullscreen.toggled,
    syncUpdate: state.game.syncUpdate,
    syncPack: state.game.syncPack,
    blockersCount: state.game.blockersCount,
    BlockerTimeout: state.networking.BlockerTimeout
});
export default connect(mapStateToProps)(Game)