const initialState = {
    lobby: false,
    impulsing: true,
    prepairing: false,
    ready: false,
    update: false,
    updatePack: {},
    syncUpdate: false,
    syncPack: {},
    blockersCount: 0
}

export default function game(state = initialState, action) {
    switch (action.type) {
        case "SetIAmLobby":
            return (
                {
                    ...state,
                    lobby: true
                }
            )
        case "SetGameImpulsing": 
            return (
                {
                    ...state,
                    impulsing: action.value
                }
            )
        case "SetGamePrepairing": 
            return (
                {
                    ...state,
                    prepairing: action.value
                }
            )
        case "SetGameReady":
            return (
                {
                    ...state,
                    ready: action.value
                }
            )
        case "CallUpdate": 
            return (
                {
                    ...state,
                    update: true,
                    updatePack: action.updatePack
                }
            )
        case "SubmitUpdate":
            return (
                {
                    ...state,
                    update: false
                }
            )
        case "CallSyncUpdate":
            return (
                {
                    ...state,
                    syncUpdate: true,
                    syncPack: action.syncPack
                }
            )
        case "SubmitSyncUpdate":
            return (
                {
                    ...state,
                    syncUpdate: false
                }
            )
        case "AddBlocker": 
            return (
                {
                    ...state,
                    blockersCount: state.blockersCount + 1
                }
            )
        case "RemoveBlocker": 
            return (
                {
                    ...state,
                    blockersCount: state.blockersCount - 1
                }
            )
        default:
            return state
    }
}
