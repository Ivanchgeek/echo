import {combineReducers} from "redux"
import transitions from "./transitions"
import networking from "./networking"
import notifications from "./notifications"
import game from "./game"
import fullscreen from "./fullscreen"
import log from "./log"

export default combineReducers({
    transitions,
    networking,
    notifications,
    game,
    fullscreen,
    log
})
