const initialState = {
    LobbyToken: "",
    BlockerTimeout: 150
}

export default function log(state = initialState, action) {
    switch (action.type) {
        case "SetLobbyToken": 
            return (
                {
                    ...state,
                    LobbyToken: action.value
                }
            )
        case "SetBlockerTimeout":
            return (
                {
                    ...state, 
                    BlockerTimeout: action.value
                }
            )
        default:
            return state
    }
}
