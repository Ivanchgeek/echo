const initialState = {
    toggled: false
}

export default function fullscreen(state = initialState, action) {
    switch (action.type) {
        case "SetFullscreenToggled": 
            return (
                {
                    ...state,
                    toggled: action.value
                }
            )
        default:
            return state
    }
}
