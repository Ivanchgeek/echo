const initialState = {
    content: ""
}

export default function notifications(state = initialState, action) {
    switch (action.type) {
        case "Notify": 
            return (
                {
                    ...state,
                    content: action.value
                }
            )
        default:
            return state
    }
}
