const initialState = {
    loading: false,
    awaitLoadingStop: false, 
    logoVisible: true
}

export default function log(state = initialState, action) {
    switch (action.type) {
        case "StartLoading":
            return (
                {
                    ...state,
                    loading: true
                }
            )
        case "StopLoading":
            return (
                {
                    ...state,
                    awaitLoadingStop: true
                }
            )
        case "HardStopLoading": 
            return (
                {
                    ...state,
                    loading: false
                }
            )
        case "SetAwaitLoadingStop":
            return (
                {
                    ...state,
                    awaitLoadingStop: action.value
                }
            )
        case "HideLogo":
                return (
                    {
                        ...state, 
                        logoVisible: false
                    }
                )
        default:
            return state
    }
}
