const initialState = {
    lines: []
}

export default function log(state = initialState, action) {
    switch (action.type) {
        case "WriteLog":
            console.log(action.value)
            return (
                {
                    ...state,
                    lines: state.lines.concat(action.value)
                }
            )
        case "RemoveFirstLog": 
            var update = [...state.lines]
            update.shift()
            return (
                {
                    ...state,
                    lines: update,
                    debug: state.debug + 1
                }
            )
        default:
            return state
    }
}
