FROM golang:latest AS go_builder
ADD . /app
WORKDIR /app/back
RUN go mod download
RUN cd cmd && CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags "-w" -a -o /main

FROM node:alpine AS node_builder
COPY --from=go_builder /app/front ./
RUN npm install
RUN npm run build

FROM alpine:latest
RUN apk --no-cache add ca-certificates
COPY --from=go_builder /main ./
COPY --from=node_builder /build ./front
RUN chmod +x ./main
CMD ./main --port=$PORT
