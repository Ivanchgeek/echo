var size = 50
var realAngle = 90
var x = 100
var y = 200
var px = 100
var py = 400
var ctx
var player
var basePoint = {x:0,y:0}
var mouseFlag = false
var pointMove = true

const redraw = () => {
    var canvas = document.getElementsByTagName('canvas')[0]
    canvas.onmousedown = () => {
        mouseFlag = true
    }
    canvas.onmouseup = () => {
        mouseFlag = false
    }
    canvas.onmouseleave = () => {
        mouseFlag = false
    }
    document.getElementById("angle").oninput = () => {
        realAngle = parseInt(document.getElementById("angle").value)
        redraw()
    }    
    document.getElementById("move").onclick = () => {
        pointMove = !pointMove
        document.getElementById("move").innerText = pointMove ? "Move square" : "Move point"
    }
    ctx = canvas.getContext(`2d`)
    player = getSquareColision()
    var result = test()
    console.log(result ? "point in" : "point out")
    canvas.style.borderColor = result ? "#62e30b" : "#cf1130"
    console.log("_________")
}

const test = () => {
    ctx.clearRect(0, 0, 300, 500)

    var sides = []
    for (let i = 0; i < 4; i++) {
        let side = {}
        let point_1 = player[i]
        let point_2 = player[i === 3 ? 0 : i + 1]
        side.range = {}
        side.range.includes = i % 2 === 0
        if (point_1.x === point_2.x) {
            side.xEquality = true
            side.b = point_1.x
            side.k = 1
            if (point_1.y < point_2.y) {
                side.range.start = point_1.y
                side.range.end = point_2.y
            } else {
                side.range.start = point_2.y
                side.range.end = point_1.y
            }
            sides.push(side)
            continue
        }
        side.xEquality = false
        side.k = (point_2.y - point_1.y) / (point_2.x - point_1.x)
        side.b = point_1.y - side.k * point_1.x
        if (point_1.x < point_2.x) {
            side.range.start = point_1.x
            side.range.end = point_2.x
        } else {
            side.range.start = point_2.x
            side.range.end = point_1.x
        }
        sides.push(side)
    }

    for (let i = 0; i < 4; i++) {
        ctx.beginPath()
        ctx.strokeStyle = "#62e30b65"
        if (sides[i].xEquality) {
            ctx.moveTo(sides[i].b, 0)
            ctx.lineTo(sides[i].b, 500)
        } else {
            ctx.moveTo(0, sides[i].k * 0 + sides[i].b)
            ctx.lineTo(300, sides[i].k * 300 + sides[i].b)
        }
        ctx.stroke()
    }

    ctx.beginPath()
    ctx.lineWidth = "2";
    ctx.strokeStyle = "#358ce8"
    ctx.moveTo(player[0].x, player[0].y)
    for (let i = 1; i < 4; i++) {
        ctx.lineTo(player[i].x, player[i].y)
    }
    ctx.lineTo(player[0].x, player[0].y)
    ctx.stroke()

    basePoint.x = sides[0].range.start + (sides[0].range.end - sides[0].range.start) / 2
    if (basePoint.x == px) {
        basePoint.x += (sides[0].range.end - sides[0].range.start) * 0.1
    }
    basePoint.y = basePoint.x * sides[0].k + sides[0].b

    var controlLine = {}
    controlLine.k = (basePoint.y - py) / (basePoint.x - px)
    controlLine.b = py - px * controlLine.k
    controlLine.incremental = basePoint.x > px

    ctx.strokeStyle = "#cf1130"
    ctx.beginPath()
    ctx.moveTo(px, py)
    ctx.lineTo(controlLine.incremental ? 300 : 0, (controlLine.incremental ? 300 : 0) * controlLine.k + controlLine.b)
    ctx.stroke()

    var intersections = 0
    for (let i = 0; i < 4; i++) {
        if (sides[i].xEquality) {
            let y = controlLine.k * sides[i].b + controlLine.b
            if ((y > sides[i].range.start && y < sides[i].range.end || (sides[i].range.includes && (y === sides[i].range.start || y === sides[i].range.end))) && (controlLine.incremental && sides[i].b > px || !controlLine.incremental && sides[i].b < px)) {
                intersections++
                ctx.fillStyle = "#62e30b"
                ctx.beginPath()
                ctx.arc(sides[i].b, y, 4, 0, 360)
                ctx.fill()
            }
        } else {
            let x = (sides[i].b - controlLine.b) / (controlLine.k - sides[i].k)
            if ((x < sides[i].range.end && x > sides[i].range.start || (sides[i].range.includes && (x === sides[i].range.start || x === sides[i].range.end))) && (controlLine.incremental && x > px || !controlLine.incremental && x < px)) {
                intersections++
                let y = x * controlLine.k + controlLine.b
                ctx.fillStyle = "#62e30b"
                ctx.beginPath()
                ctx.arc(x, y, 4, 0, 360)
                ctx.fill()
            }
        }
    }
    console.log(`sides: `)
    console.log(sides)
    console.log("control: ")
    console.log(controlLine)
    console.log("intersections:" + intersections)
    showResults(sides, controlLine, intersections, basePoint, {x: px, y: py})
    return intersections % 2 === 1
}

getSquareColision = () => {
    let angle = realAngle % 90
    var baseX = x + size * angle / 90
    var baseY = -Math.sqrt(Math.pow(size, 2) / 2 - Math.pow((size * (angle / 90 - 0.5)), 2)) + y + size / 2
    return [
        {
            x: baseX, 
            y: baseY
        }, 
        {
            x: baseX + size * Math.cos(angle * Math.PI / 180), 
            y: baseY + size * Math.sin(angle * Math.PI / 180)
        }, 
        {
            x: x + size - (baseX - x), 
            y: y + size + (y - baseY)
        }, 
        {
            x: x - (baseX + size * Math.cos(angle * Math.PI / 180) - (x + size)),
            y: y + size - (baseY + size * Math.sin(angle * Math.PI / 180) - y)
        }
    ]
}

window.onload = redraw

window.onmousemove = () => {
    if (mouseFlag) {
        if (pointMove) {
            px = event.pageX
            py = event.pageY
        } else {
            x = event.pageX
            y = event.pageY
        }
        redraw()
    }
}

const showResults = (sides, control, intersections, basePoint, startPoint) => {
    var out = document.getElementById("result")
    var text = `
        square position:  x: ${x}, y: ${y}
        angle: ${realAngle}
        startPoint: x: ${startPoint.x}, y: ${startPoint.y}
        basePoint: x: ${basePoint.x}, y: ${basePoint.y}
        sides:\n`
    for (let i = 0; i < sides.length; i++) {
        if (sides[i].xEquality) {
            text += `${i+1}) x=${sides[i].b} y∈${sides[i].range.includes ? "[" : "("}${sides[i].range.start}; ${sides[i].range.end} ${sides[i].range.includes ? "]" : ")"}\n`
        } else {
            text += `${i+1}) y=${sides[i].k}*x+${sides[i].b} x∈${sides[0].range.includes ? "[" : "("}${sides[i].range.start}; ${sides[i].range.end} ${sides[0].range.includes ? "]" : ")"}\n`
        }
    }
    text += `control line: y=${control.k}*x+${control.b} x∈`
    if (control.incremental) {
        text += `[${px}; +∞)`
    } else {
        text += `(-∞; ${px}]`
    }
    text += `\nintersections: ${intersections}`
    out.innerText = text
}